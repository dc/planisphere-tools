#!/usr/bin/env python2

# Written by Sean Dilda <sean@duke.edu>
# Copyright 2017 Duke University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys
import argparse
import platform
import json
import glob
import re
import subprocess
import urllib2
import time
import ConfigParser

# Using chassis types as defined in https://www.dmtf.org/sites/default/files/standards/documents/DSP0119.pdf section 3.3.4.1
chassis_type_map = {
    '3': 'desktop',
    '4': 'desktop',
    '5': 'desktop',
    '6': 'desktop',
    '7': 'desktop',
    '8': 'laptop',
    '9': 'laptop',
    '10': 'laptop',
    '11': 'laptop',
    '13': 'desktop',
    '14': 'laptop',
    '17': 'server_physical',
    '21': 'laptop',
    '23': 'server_physical',
    '25': 'server_physical', # blade
}

def get_linux_arm_info(data):
    # These boards don't tend to use DMI, so its a bit trickier to get good
    # HW info.

    # This comes from http://elinux.org/RPi_HardwareHistory
    raspberry_models = {
        '0002': 'B',
        '0003': 'B',
        '0004': 'B',
        '0005': 'B',
        '0006': 'B',
        '0007': 'A',
        '0008': 'A',
        '0009': 'A',
        '000d': 'B',
        '000e': 'B',
        '000f': 'B',
        '0010': 'B+',
        '0011': 'Compute Module 1',
        '0012': 'A+',
        '0013': 'B+',
        '0014': 'Compute Module 1',
        '0015': 'A+',
        'a01040': '2 Model B',
        'a21041': '2 Model B',
        'a21041': '2 Model B',
        'a22042': '2 Model B',
        '900021': 'A+',
        '900032': 'B+',
        '900092': 'Zero',
        '900093': 'Zero',
        '920093': 'Zero',
        '9000c1': 'Zero W',
        'a02082': '3 Model B',
        'a020a0': 'Compute Module 3',
        'a22082': '3 Model B',
        'a32082': '3 Model B',
    }
    for mac in data['mac_addresses']:
        if mac.startswith('b8:27:eb'):
            # its a raspberry pi
            data['manufacturer'] = 'Raspberry Pi'
            for line in open('/proc/cpuinfo', 'r'):
                line = line.strip()
                if line == '':
                    continue
                key, value = line.split(':', 1)
                key = key.strip()
                if key == 'Revision':
                    try:
                        data['model'] = raspberry_models[value.strip()]
                    except KeyError:
                        pass
                elif key == 'Serial':
                    data['serial'] = value.strip()


def get_machine_info():
    system, node, release, version, machine, processor = platform.uname()
    info = {
        'last_active': time.strftime('%Y-%m-%d %H:%M:%S %z'),
        'data': {},
    }
    data = info['data']
    data['os_family'] = system
    data['hostname'] = node
    if data['os_family'] == 'Linux':
        info['key'] = safe_read('/etc/machine-id') or safe_read('/var/lib/dbus/machine-id')
        # Planisphere strips out MAC addresses that are all 0's or have the
        # local bit set, so we don't have to worry about that here.
        data['mac_addresses'] = [open(fn, 'r').read().strip() for fn in glob.iglob('/sys/class/net/*/address')]
        data['mac_addresses'] = filter(None, data['mac_addresses']) # remove empty strings
        data['manufacturer'] = safe_read('/sys/class/dmi/id/sys_vendor')
        data['model'] = safe_read('/sys/class/dmi/id/product_name')
        data['serial'] = safe_read('/sys/class/dmi/id/product_serial')
        try:
            data['device_type'] = chassis_type_map[safe_read('/sys/class/dmi/id/chassis_type')]
        except KeyError:
            pass
        data['os_fullname'] = read_osname()
        if data['os_fullname'] and re.search("fedora|red hat|centos|scientific", data['os_fullname'], re.IGNORECASE):
            data['installed_software'] = retrieve_packages_rpm()
        elif data['os_fullname'] and re.search("wheezy", data['os_fullname'], re.IGNORECASE):
            data['installed_software'] = retrieve_packages_dpkg()
        elif data['os_fullname'] and re.search("debian|ubuntu|raspbian", data['os_fullname'], re.IGNORECASE):
            data['installed_software'] = retrieve_packages_apt()
        elif data['os_fullname'] and re.search("arch", data['os_fullname'], re.IGNORECASE):
            data['installed_software'] = retrieve_packages_pacman()

        if machine == 'armv7l':
            get_linux_arm_info(data)

        # Add serial to key
        if info['key'] and data['serial']:
            info['key'] = '%s-%s' % (data['serial'], info['key'])

    return info

def post_results(info, config, verbose):
    if not os.access(config['key-file'], os.R_OK):
        print >> sys.stderr, 'Error: Cannot read %s' % (config['key-file'])
        sys.exit(5)
    key = open(config['key-file'], 'r').read().strip()
    if not key:
        print >> sys.stderr, 'Error: %s cannot be empty' % (config['key-file'])
        sys.exit(6)

    req = urllib2.Request(config['url'])
    req.add_header('Content-Type', 'application/json')
    req.add_header('PLANISPHERE-REPORT-KEY', key)
    req.add_header('User-Agent', 'planisphere-report')
    try:
        response = urllib2.urlopen(req, json.dumps(info))
        body = response.read()
    except urllib2.URLError as e:
        if 'read' in dir(e):
            # We still want the body when there's an error
            body = e.read()
        else:
            raise
    results = json.loads(body)
    if results['status'] == 'success':
        if verbose:
            print 'Device URL: %s' % (results['device_url'])
        sys.exit(0)
    else:
        print 'Error from planisphere: %s' % (results['error_msg'])
        sys.exit(10)

def read_osname():
    try:
        for line in open('/etc/os-release', 'r'):
            key, val = line.split('=',2)
            if key == 'PRETTY_NAME':
                val = val.strip()
                if val[0] == '"' and val[-1] == '"':
                    val = val[1:-1]
                return val
    except IOError:
        pass

    # Now try /etc/system-release
    return safe_read('/etc/system-release')

def retrieve_packages_apt():
    output = subprocess.Popen(['apt', 'list', '--installed'], stdout=subprocess.PIPE).communicate()[0]
    packages = []
    for line in output.splitlines():
        if line == 'Listing...':
            continue
        name, extra = line.split('/', 1)
        repo, version, extra = line.split(' ', 2)
        packages.append([name, version])
    return packages

def retrieve_packages_rpm():
    output = subprocess.Popen(['rpm', '-qa', '--qf', '%{NAME} %|EPOCH?{%{EPOCH}:}:{}|%{VERSION}-%{RELEASE}\n'], stdout=subprocess.PIPE).communicate()[0]
    return [line.split(' ', 2) for line in output.splitlines()]

def retrieve_packages_dpkg():
    output = subprocess.Popen(['dpkg-query', '-W'], stdout=subprocess.PIPE).communicate()[0]
    return [line.split('\t', 2) for line in output.splitlines()]

def retrieve_packages_pacman():
    output = subprocess.Popen(['pacman', '-Q'], stdout=subprocess.PIPE).communicate()[0]
    return [line.split(' ', 2) for line in output.splitlines()]


def safe_read(fn):
    try:
        results = open(fn, 'r').read().strip()
    except IOError:
        results = None
    return results

def main():
    # Parse arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--dry-run', help="Print data that would be sent to planisphere, but don't send it", action="store_true")
    parser.add_argument('-v', '--verbose', help="Print success output after posting to planisphere", action="store_true")
    parser.add_argument('-c', '--config', help="Override the default config file location", default="/etc/planisphere-report")
    args = parser.parse_args()

    # Parse config
    overrides = {}
    config = {
        'key-file': '/etc/planisphere-report-key',
        'url': 'https://planisphere.oit.duke.edu/self_report',
    }
    parser = ConfigParser.RawConfigParser()
    parser.read(args.config)
    if parser.has_section('config'):
        config.update(parser.items('config'))
    if parser.has_section('overrides'):
        overrides.update(parser.items('overrides'))

    info = get_machine_info()
    # Apply overrides
    info['data'].update(overrides)

    if args.dry_run:
        print json.dumps(info, sort_keys=True, indent=4)
    else:
        post_results(info, config, args.verbose)


if __name__ == "__main__":
    main()
